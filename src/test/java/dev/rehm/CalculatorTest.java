package dev.rehm;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {

    private Calculator calculator = new Calculator();

    @Test
    public void oneNumberReturnsItself(){
        int actual = calculator.add("5");
        int expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    public void emptyStringReturnsZero(){
        int actual = calculator.add("");
        int expected = 0;
        assertEquals(expected, actual);
    }

    @Test
    public void twoNumbersReturnSum(){
        int actual = calculator.add("4,2"); // -> ["4", "2"]
        int expected = 6;
        assertEquals(expected, actual);
    }

    @Test
    public void doubleDigitNumbersReturnSum(){
        int actual = calculator.add("40,20"); // -> ["40", "20"]
        int expected = 60;
        assertEquals(expected, actual);
    }



}
