package dev.rehm;

public class Calculator {

    /*
        write an add method which accepts a string and returns an int

        we want the method to take 0, 1, or 2 numbers separated by a comma

        - provide 0 numbers (an empty string), method returns 0
        - provide 1 number, method returns that number
        - provide 2 numbers, method to return their sum

        additional requirements:
        - test for invalid input (null,  non numeric), return -1
        - ignore any values that are less than 0
        - allow add method to handle an unknown amount of numbers
     */

    public int add(String input){
        if(input==null || input.isEmpty()){
            return 0;
        }
        String[] numArr = input.split(",");

        if(numArr.length == 2 ) {
            return Integer.parseInt(numArr[0]) + Integer.parseInt(numArr[1]);
        }

        // convert string to an int
        return Integer.parseInt(input);
    }

}
